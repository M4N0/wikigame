from bs4 import BeautifulSoup
import urllib.request
import os


# On crée une fonction qui vérifie la réponse de l'utilisateur
def answer(max):
    print()
    x = ""
    while x.isdigit() == False or int(x) > max or int(x) < 1:
        print('\033[36m', "Veuillez saisir le lien à suivre : ", '\033[0m')
        x = input()
    print()
    return int(x)


# https://fr.wikipedia.org/wiki/1983            (Lien de test)
# https://en.wikipedia.org/wiki/Special:Random  (Lien random)
with urllib.request.urlopen('https://en.wikipedia.org/wiki/Special:Random') as response:
    webpage = response.read()
    soup = BeautifulSoup(webpage, 'html.parser')

# On récupère le titre de la page à atteindre
FinalPageTitle = soup.find(id="firstHeading").string

# https://fr.wikipedia.org/wiki/Bom_Jesus_do_Tocantins_(Par%C3%A1)     (Lien de test)
# https://en.wikipedia.org/wiki/Special:Random                         (Lien random)
with urllib.request.urlopen('https://en.wikipedia.org/wiki/Special:Random') as response:
    webpage = response.read()
    soup = BeautifulSoup(webpage, 'html.parser')

# On récupère le titre de la page actuelle
actualPageTitle = soup.find(id="firstHeading").string

game = True
coups = 0

while game == True:

    # On efface l'affichage de la console précédant le tour
    os.system('clear')
    os.system('cls')

    # Récapitulatif du tour
    print('\033[36m')  # Texte Vert
    print("=================== Tour n.", coups + 1, " ===================")
    print('\033[0m')  # Retour à la couleur par défaut
    print('\033[32m', "Page de actuelle : ", '\033[0m', actualPageTitle)
    print('\033[35m', "Page à atteindre : ", '\033[0m', FinalPageTitle)
    print()

    # On récupère le contenu de l'élément contenant les liens désirés
    for content in soup.find_all('div', {"class": "mw-parser-output"}):
        LinksList = []
        urlList = []
        i = 0
        print("Voici les liens disponibles de cette page :")
        print()

        # On récupère les liens (balises "a")
        for link in content.find_all('a'):

            # On fait le tri des liens pour ne garder que les liens désirés
            if link.get("title") not in LinksList and link.parent.name != "th" and link.parent.name != "td" and link.get("title") != None and ':' not in link.get("href") and '#' not in link.get("href") and '/w/index.php' not in link.get("href"):
                i = i + 1
                LinksList.append(link.get("title"))
                urlList.append(link)
                print('\033[33m', i, " ", '\033[0m',  LinksList[i-1])

    # En s'aidant de la fonction answer(), on demande au joueur de saisir sa réponse
    lienSuivi = answer(i) - 1
    print('\033[33m', "Vous avez suivi ce lien : ",
          LinksList[lienSuivi], '\033[0m')
    print()
    coups = coups + 1

    # On se dirige vers la page désirée
    with urllib.request.urlopen("https://wikipedia.org/" + urlList[lienSuivi].get("href")) as response:
        webpage = response.read()
        soup = BeautifulSoup(webpage, 'html.parser')
        actualPageTitle = soup.find(id="firstHeading").string

    # Si le joueur arrive sur la page à atteindre, alors on sort de la boucle et il gagne
    if actualPageTitle == FinalPageTitle:
        game = False

print('\033[35m' + " ===== Félicitations! Vous avez réussi en",
      coups, "coups ===== ", '\033[0m')
